Welcome to Alaska!
This is CRUD service for bears in alaska.
CRUD routes presented with REST naming notation:

- POST /bear - create
- GET /bear - read all bears
- GET /bear/:id - read specific bear
- PUT /bear/:id - update specific bear
- DELETE /bear - delete all bears
- DELETE /bear/:id - delete specific bear

- Example of ber json: {"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5}.
- Available types for bears are: POLAR, BROWN, BLACK and GUMMY.

1. Installation (Example for Fedora):
- sudo dnf install -y python3
- curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
- python get-pip.py
- pip3 install pytest
- pip3 install requests

2. Docker:
- docker pull azshoo/alaska
- docker run -p 8091:8091 azshoo/alaska:1.0
- Open browser and check http://0.0.0.0:8091/info

3. Test Alaska:
- git clone https://gitlab.com/Stanse/test_rest_alaska.git
- cd test_rest_alaska/test
- py.test -v test_Alaska.py

