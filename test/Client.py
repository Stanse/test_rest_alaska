import Bear
import requests
import json


class Client:
    host = None
    headers = {'Content-Type': 'application/json'}
    counter = 0

    def __init__(self, host="http://localhost", port='8091'):
        self.host = host + ':' + port + '/bear'
        self.r = requests.session()

    def create_bear(self, new_bear):
        response = self.r.post(self.host, headers=self.headers, data=json.dumps(new_bear.__dict__))
        if response.ok:
            Client.counter += 1
        return response

    def update_bear(self, bear_id, bear):
        self.host += '/' + str(bear_id)
        response = self.r.put(self.host, headers=self.headers, data=json.dumps(bear.__dict__))
        return response

    def get_all_bear(self):
        response = self.r.get(self.host)
        return response

    def get_bear(self, bear_id):
        self.host += '/' + str(bear_id)
        response = self.r.get(self.host)
        return response

    def delete_all_bear(self):
        response = self.r.delete(self.host)
        if response.ok:
            Client.counter = 0
        return response

    def delete_bear(self, bear_id):
        self.host += '/' + str(bear_id)
        response = self.r.delete(self.host)
        if response.ok:
            Client.counter -= 1
        return response
