import Client
import Bear
import pytest
import json


@pytest.fixture()
def get_client():
    return Client.Client()


class Test_Alaska:

    last_id = None
    last_name = None
    last_type = None
    last_age = None

    def teardown_class(self, get_client):
        cl = get_client
        cl.delete_all_bear()

    def test_delete_all_bears(self, get_client):
        cl = get_client
        response = cl.delete_all_bear()
        if not response.ok:
            pytest.fail("ERROR: Status code " + str(response.status_code))
        assert response.ok

    @pytest.mark.parametrize("args", [
        pytest.param(("BROWN", "John", 99.7), id="BROWN bear"),
        pytest.param(("BROWN", "din", 85.7), id="BROWN bear"),
        pytest.param(("BROWN", "sAm", 71.7), id="BROWN bear"),
        pytest.param(("BROWN", "Bobby", 60.2), id="BROWN bear"),
        pytest.param(("BLACK", "ruBBy", 57), id="BLACK bear"),
        pytest.param(("BLACK", "Alastar", 37), id="BLACK bear"),
        pytest.param(("BLACK", "Crowley", 44), id="BLACK bear"),
        pytest.param(("BLACK", "Rowena", 0.35), id="BLACK bear"),
        pytest.param(("GUMMY", "MASHA", 17), id="GUMMY bear"),
        pytest.param(("GUMMY", "Sasha", 19), id="GUMMY bear"),
        pytest.param(("GUMMY", "Dasha", 0.3), id="GUMMY bear"),
        pytest.param(("GUMMY", "Lena", 24), id="GUMMY bear"),
        pytest.param(("POLAR", "Misha", 12.2), id="POLAR bear"),
        pytest.param(("POLAR", "Michael", 28.2), id="POLAR bear"),
        pytest.param(("POLAR", "Fenya", 7.2), id="POLAR bear"),
        pytest.param(("POLAR", "Sonya", 4.2), id="POLAR bear"),
    ])
    def test_add_and_get_new_bear_success(self, get_client, args):
        cl = get_client
        new_bear = Bear.Bear(*args)
        response = cl.create_bear(new_bear)
        if not response.ok:
            pytest.fail("ERROR: Status code " + str(response.status_code))
        current_id = int(response.text)
        tmp_bear = cl.get_bear(current_id)
        name = str(new_bear.bear_name).upper()
        try:
            tmp_bear = json.loads(str(tmp_bear.text))
            tmp_id = tmp_bear["bear_id"]
        except Exception as e:
            pytest.fail("ERROR: Can't get id:" + str(e))
        if current_id != tmp_id:
            pytest.fail("ERROR: Wrong id: " + current_id + " != " + tmp_id)
        if new_bear.bear_type != tmp_bear["bear_type"]:
            pytest.fail("ERROR: Wrong type: " + new_bear.bear_type + " != " + tmp_bear["bear_type"])
        if name != tmp_bear["bear_name"]:
            pytest.fail("ERROR: Wrong name: " + name + " != " + tmp_bear["bear_name"])
        if new_bear.bear_age != tmp_bear["bear_age"]:
            pytest.fail("ERROR: Wrong age: " + new_bear.bear_age + " != " + tmp_bear["bear_age"])

    def test_get_all_bears(self, get_client):
        cl = get_client
        response = cl.get_all_bear()
        bears_list = json.loads(str(response.text))
        num_of_bears = len(bears_list)
        assert num_of_bears == cl.counter

    @pytest.mark.parametrize("age", [
        pytest.param(("BROWN", "JOHN", 120.7), id="BROWN bear"),
        pytest.param(("BLACK", "BOBBY", 257), id="BLACK bear"),
        pytest.param(("GUMMY", "VOVAN", 317), id="GUMMY bear"),
        pytest.param(("POLAR", "MISHA", 412.2), id="POLAR bear"),
    ])
    def test_add_100_years_old_bear(self, get_client, age):
        cl = get_client
        cl.delete_all_bear()
        new_bear = Bear.Bear(*age)
        response = cl.create_bear(new_bear)
        if not response.ok:
            pytest.fail("ERROR: Status code " + str(response.status_code))
        current_id = int(response.text)
        tmp_bear = cl.get_bear(current_id)
        tmp_bear = json.loads(str(tmp_bear.text))
        tmp_age = tmp_bear["bear_age"]
        assert tmp_age == 0.0

    def test_add_negative_values(self, get_client):
        cl = get_client
        new_bear = Bear.Bear("POLAR", "mikhail", -22.3)
        response = cl.create_bear(new_bear)
        if not response.ok:
            pytest.fail("ERROR: Status code " + str(response.status_code))
        current_id = int(response.text)
        tmp_bear = cl.get_bear(current_id)
        tmp_bear = json.loads(str(tmp_bear.text))
        tmp_age = tmp_bear["bear_age"]
        assert tmp_age == 0.0

    def test_add_string_age(self, get_client):
        cl = get_client
        new_bear = Bear.Bear("POLAR", "mikhail", "22.3")
        response = cl.create_bear(new_bear)
        if not response.ok:
            pytest.fail("ERROR: Status code " + str(response.status_code))
        current_id = int(response.text)
        tmp_bear = cl.get_bear(current_id)
        tmp_bear = json.loads(str(tmp_bear.text))
        tmp_age = tmp_bear["bear_age"]
        assert tmp_age == 22.3

    def test_add_wrong_age(self, get_client):
        cl = get_client
        new_bear = Bear.Bear("POLAR", "mikhail", "-aSDf;lkj")
        response = cl.create_bear(new_bear)
        assert response.status_code == 500

    def test_add_wrong_type(self, get_client):
        cl = get_client
        new_bear = Bear.Bear("PANDA", "mikhail", 11)
        response = cl.create_bear(new_bear)
        assert response.status_code == 500

    def test_put_type_by_id(self, get_client):
        cl = get_client
        cl.delete_all_bear()
        bear = Bear.Bear("POLAR", "GOGA", 22.3)
        response = cl.create_bear(bear)
        current_id = int(response.text)
        # bear = Bear.Bear("BLACK", "KROKODIL", 25.3)
        bear.bear_type = "BLACK"
        response = cl.update_bear(current_id, bear)
        Test_Alaska.last_id = current_id
        Test_Alaska.last_type = bear.bear_type
        assert response.ok

    def test_get_type_after_update(self, get_client):
        cl = get_client
        bear = cl.get_bear(Test_Alaska.last_id)
        bear = json.loads(str(bear.text))
        assert Test_Alaska.last_type == bear["bear_type"]

    def test_put_name_by_id(self, get_client):
        cl = get_client
        cl.delete_all_bear()
        bear = Bear.Bear("POLAR", "GOGA", 22.3)
        response = cl.create_bear(bear)
        current_id = int(response.text)
        # bear = Bear.Bear("BLACK", "KROKODIL", 25.3)
        bear.bear_name = "KROKODIL"
        response = cl.update_bear(current_id, bear)
        Test_Alaska.last_id = current_id
        Test_Alaska.last_name = bear.bear_name
        assert response.ok

    def test_get_name_after_update(self, get_client):
        cl = get_client
        bear = cl.get_bear(Test_Alaska.last_id)
        bear = json.loads(str(bear.text))
        assert Test_Alaska.last_name == bear["bear_name"]

    def test_put_age_by_id(self, get_client):
        cl = get_client
        cl.delete_all_bear()
        bear = Bear.Bear("POLAR", "GOGA", 22.3)
        response = cl.create_bear(bear)
        current_id = int(response.text)
        # bear = Bear.Bear("BLACK", "KROKODIL", 25.3)
        bear.bear_age = 30
        response = cl.update_bear(current_id, bear)
        Test_Alaska.last_id = current_id
        Test_Alaska.last_age = bear.bear_age
        assert response.ok

    def test_get_age_after_update(self, get_client):
        cl = get_client
        bear = cl.get_bear(Test_Alaska.last_id)
        bear = json.loads(str(bear.text))
        assert Test_Alaska.last_age == bear["bear_age"]